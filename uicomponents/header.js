import React from 'react';
import PropTypes from 'prop-types';
import Alert from 'react-bootstrap/lib/Alert';
import Button from 'react-bootstrap/lib/Button';

const headerStyle = 
{
	color : 'red'
}

const Header = ({caption }) =>
(
	<div>
		<h2 style={headerStyle}> {caption} </h2>
		<Button >This is my Bootstrap Button </Button>
	</div>
)

Header.PropTypes = {
	caption : PropTypes.string.isRequired
}

export default Header;
