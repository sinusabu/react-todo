import React from 'react';
import PropTypes from 'prop-types';
import {ListGroup} from 'react-bootstrap';

/*Use {} in import when it's not default export. use import without {} when it's default export*/
import Todo from './todo';

const TodoList = ({todos, todoDeleteClick, todoToggleClick}) => (
	<ListGroup>
		<h2>TODOS</h2>
		{todos.map(
			todo => (
				console.log('TODOs are rendered again ' + todo.text))
		)}		
		{todos.map(
			todo => (
				<Todo key={todo.id} id={todo.id} text={todo.text} 
					completed={todo.completed} todoDeleteClick={todoDeleteClick}
					todoToggleClick={todoToggleClick}
					/>				
			)
		)}					
						
	</ListGroup> 
);

TodoList.PropTypes = {
	todos : PropTypes.arrayOf(
		PropTypes.shape(
		{ 
			id: PropTypes.number.isRequired,
			text : PropTypes.string.isRequired,
			completed : PropTypes.bool.isRequired,
			todoDeleteClick : PropTypes.func.isRequired	,
			todoToggleClick : PropTypes.func.isRequired	
		}
		).isRequired
	).isRequired
};

export default TodoList;
