import React from 'react';
import ReactDOM from 'react-dom';
import Header from './header.js';
import Details from './details.js';

const root = () =>
(
	<div>
		<Header caption ="This is my private TO-DO application" />
		<hr />

		<Details />
	</div>
)

export default root;