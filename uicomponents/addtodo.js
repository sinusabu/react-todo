import React from 'react'
import PropTypes from 'prop-types'
import {addTodo}  from '../actions/todoactions'

const AddToDoUI = ({dispatch}) => {
	let textInput

	return (<form 
		onSubmit =
		{
			e=> {
				e.preventDefault()
				
				if (!textInput)
				{
					console.log('Invalid text')
					return
				}
				if (!textInput.value.trim())
				{
					console.log('Invalid text')
					return
				}
				console.log(textInput.value)
				dispatch(addTodo(textInput.value.trim()));
				console.log('dispatched');
				textInput.value = ''
			}
		}	
	
	>
		<input type="text" id="tt" ref={input => textInput = input} /> 
		<input type="Submit" value= "Add To-Do"   />
	</form>)
}

AddToDoUI.PropTypes = {
	text : PropTypes.string.isRequired,
	onclick : PropTypes.func.isRequired
}

export default AddToDoUI
