import React from 'react'
import PropTypes from 'prop-types'

import AddToDoConnect from '../containercomponents/addtodostoreconnect'
import VisibleTodos from '../containercomponents/todosstoreconnect'

const Details = () => (
	<div>
		This is the details section
		<AddToDoConnect />
		<VisibleTodos />
		
	</div>

)

export default Details;
