import React from 'react';
import PropTypes from 'prop-types';
import {ListGroupItem} from 'react-bootstrap';

const Todo = ({id, text, completed, todoDeleteClick, todoToggleClick}) => (
			<ListGroupItem>
				<b>
				{text} 
				</b>
				<a id="completeclick" href= "#" onClick={() => todoToggleClick(id)}> 
						<div style={{textDecoration: completed ? 'line-through':'none'}}>Completed </div></a>  
				<a id="deleteclick" href= "#" onClick={() => todoDeleteClick(id)}>   Delete </a> <br />
			</ListGroupItem>
); 

Todo.PropTypes = {
	id: PropTypes.number.isRequired,
	text : PropTypes.string.isRequired,
	completed : PropTypes.bool.isRequired,
	todoDeleteClick : PropTypes.func.isRequired,
	todoToggleClick : PropTypes.func.isRequired
};

export default Todo;
