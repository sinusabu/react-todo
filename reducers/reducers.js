/*
	(previousState, action) => newState

	Dont do any of below inside a reducer

    1. Mutate its arguments;
    2. Perform side effects like API calls and routing transitions;
    3. Call non-pure functions, e.g. Date.now() or Math.random().
	Given the same arguments, it should calculate the next state and 
	return it. No surprises. No side effects. No API calls. 
	No mutations. Just a calculation.
*/

import {visibilityFilters} from '../actions/todoactions';

const initialState = {
	visibilityFilter : visibilityFilters.showAll,
	todos : []
};

const todoReducer = (state = initialState, action) =>{
	/*if (typeof state === 'undefined'){
		return initialState;
	}    This is same as the default state in the arguments */
	console.log('ACTION :' + action.type);
	console.log('ID :' + action.id);
	console.log('STATE.TODOS :' + state.todos);
	
	switch(action.type)
	{
		case 'ADD_TODO':
			return Object.assign({}, 
				{
					visibilityFilter : state.visibilityFilter,
					todos :
					[
						...state.todos,
						{
							text : action.text,
							id : action.id,
							completed : false
						}
					]
			}) // new object is returned intead of tampering the original state object
		case 'DELETE_TODO':
			return Object.assign({}, {
					visibilityFilter : state.visibilityFilter,
					todos: [...state.todos.splice(state.todos.indexOf(action.id), 1)]
				}
			);
		case 'TOGGLE_TODO' :
			return Object.assign({}, {
					visibilityFilter : state.visibilityFilter,
					todos: [...state.todos.map(todo => (todo.id===action.id) ? {todo, completed : !todo.completed} : todo) ]				
				}
			);

		/*case CHANGE_TODO:
			newstate = state;
			if ((action.index >= 0) && (action.index < state.todos.length))
			{
				objecttoedit = object.assign({}, ...state);
				objecttoedit[index].text = action.text;
				newstate = object.assign({}, ...objecttoedit);
			}					
			return newstate;  
		case FINISH_TODO :
			newstate = state;
			if ((action.index >= 0) && (action.index < state.todos.length))
			{
				objecttoedit = object.assign({}, ...state);
				objecttoedit[index].completed = true;
				newstate = object.assign({}, ...objecttoedit);
			}					
			return newstate;
		case SET_FILTER:
			objecttoedit = object.assign({}, ...state);			
			objecttoedit.visibilityFilter = action.filter;

			return objecttoedit;*/
		default :
			return state; // return default state
	}
	console.log(state.todo + '   '+ state.visibilityFilter);
};

export default todoReducer;

