var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.join(__dirname + '/dist/');
console.log(BUILD_DIR);
var APP_DIR = path.join(__dirname );
console.log(APP_DIR);
console.log(path.join(APP_DIR , 'index.js'));

var config = {
	entry : path.join(APP_DIR , 'index.js'),
	output : 	{
			path : BUILD_DIR, 
			filename : 'bundle.js'
	},
	module:{
		loaders:[
			{
				test: /\.jsx?$/,
				exclude:/node_modules/,
				loader:'babel-loader',
				query:{
					presets:['es2015','react']
				}
			}
		]
	},
	resolve: {
    	modules: [
      		path.join(__dirname, 'node_modules'),
    		],
		extensions: ['.jsx', '.js']
  	},	
	plugins: [
		new webpack.ProvidePlugin({
            "React": "react"
        }),
		new webpack.ProvidePlugin({
            "ReactDOM": "react-dom"
        })        
    ]		
};

console.log('Output path ' + config.output.path);
console.log('Output Public path ' + config.output.PublicPath);

module.exports = config;