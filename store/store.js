/*There should be only one store in an app.
You can use multiple reducers */
import {createStore, applyMiddleware} from 'redux'
//import {createLogger} from "redux-logger"
//import promise from "redux-promise-middleware"

import todoReducer from '../reducers/reducers'

//const middleware = applyMiddleware(promise(), createLogger());

//store parameters : reducers , initial state, middleware

const todoStore = createStore(todoReducer) //, middleware)

export default todoStore

