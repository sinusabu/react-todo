import React from 'react';
import {connect} from 'react-redux';

/*Use {} in import when it's not default export. use import without {} when it's default export*/
import {addTodo}  from '../actions/todoactions';
import AddToDoUI from '../uicomponents/addtodo';

/*
const mapDispatchToProps = (dispatch) => 
{
	return {onclick : text => dispatch(addTodo(text))
	}
}
*/

//const AddToDoConnect = connect(null, mapDispatchToProps)(AddToDoUI)
const AddToDoConnect = connect()(AddToDoUI);

export default AddToDoConnect;


