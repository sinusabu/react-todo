import React from 'react';
import {connect} from 'react-redux';

/*Use {} in import when it's not default export. use import without {} when it's default export*/
import {deleteTodo} from '../actions/todoactions';
import {toggleTodo} from '../actions/todoactions';
import TodoList from '../uicomponents/todolist';

const mapStateToProps = state => {	
	console.log('Inside todo list store update :' + state.todos);
	return{ todos : state.todos};	
};

const mapDispatchToProps = dispatch => {
	return {todoDeleteClick : id =>   dispatch(deleteTodo(id)) ,
			todoToggleClick : id =>   dispatch(toggleTodo(id))
	}
};

const visibleTodos = connect(
	mapStateToProps,
	mapDispatchToProps)(TodoList);
console.log ('cal todo list connect called');

export default visibleTodos;

