//Action creators

export const visibilityFilters = {
	showAll : 'SHOW_ALL',
	completed : 'COMPLETED',
	nonCompleted : 'NON_COMPLETED'
};

let nextTodoID = 0;

export const addTodo = text => {
	return {
		type : 'ADD_TODO',
		id : nextTodoID++,
		text
	} 
};

export const deleteTodo = id => {
	return {
		type: 'DELETE_TODO',
		id
	}
};

export const toggleTodo = id => {
	return{
		type: 'TOGGLE_TODO',
		id
	}
};
/*export function addTodo(text)
{
	return
	{
		type: 'ADD_TODO',
		text
	} 
}

export function deleteTodo(id)
{
	return
	{
		type: 'DELETE_TODO',
		id
	}
}*/


/*export function changeText(index, text)
{
	return
	{
		type : CHANGE_TODO,
		index,
		text
	}
}

export function toggleTodo(index)
{
	return
	{
		type :TOGGLE_TODO,
		index
	}
}

export function finishTodo(index)
{
	return
	{
		type : FINISH_TODO,
		index
	}
}

export function setFilter(filter)
{
	return
	{
		type : SET_FILTER,
		filter
	}
}

*/