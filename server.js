const express = require('express');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpack = require('webpack');
const webpackConfig = require('./webpack.config.js');
const app = express();
var path = require('path');
 
const compiler = webpack(webpackConfig);
 
app.use(express.static(webpackConfig.output.path));

app.use(webpackDevMiddleware(compiler, {
  hot: true,  
  path : '/',
  publicPath: webpackConfig.output.path,  
  filename: 'bundle.js',
  log: console.log,
  stats: {
    colors: true,
  },
  historyApiFallback: true,
}));
 
const server = app.listen(3000, function() {
  const host = server.address().address;
  const port = server.address().port;
  console.log('Example app listening at http://%s:%s', host, port);
});