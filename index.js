import React from 'react';
import ReactDOM from 'react-dom';
import RootComponent from './uicomponents/root.js'
import {Provider} from 'react-redux';
import todoStore from './store/store.js';

ReactDOM.render(
	<Provider store={todoStore}>
		<RootComponent /> 
	</Provider>	, 
	document.getElementById('root'))
